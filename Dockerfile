FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:20191002

RUN yum install -y \
    python3-pip \
    python36-devel \
    gcc \
    curl

# Need newer version of Nodejs than is available by default
RUN curl -sL https://rpm.nodesource.com/setup_13.x | bash -
RUN yum install -y nodejs

# Install CUDA toolkit to run GPU use cases
RUN curl -O \
    https://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-repo-rhel7-10.1.105-1.x86_64.rpm
RUN rpm -i cuda-repo-rhel7-10.1.105-1.x86_64.rpm
RUN yum clean all
RUN yum install -y cuda-10.1.105-1

RUN pip3 install --no-cache --upgrade pip && \
    pip install --no-cache jupyterhub==1.1.0 && \
    pip install --no-cache jupyterlab==1.1.4 && \
    pip install --no-cache notebook==6.0.3 && \
    pip install --no-cache bokeh==1.4.0 && \
    pip install --no-cache pynvml==8.0.4 && \
    pip install --no-cache jupyterlab-nvdashboard==0.2.0 && \
    pip install --no-cache numba==0.48.0 && \
    pip install --no-cache psutil>=5.6.0 && \
    pip install --no-cache numpy && \
    pip install --no-cache nbresuse

RUN jupyter labextension install jupyterlab-nvdashboard

ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}
ENV SHELL bash

RUN adduser --uid ${NB_UID} ${NB_USER}

# Guarantee that the contents of the repo are in ${HOME}
COPY *.ipynb ${HOME}
USER root
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}

WORKDIR ${HOME}
USER ${USER}
