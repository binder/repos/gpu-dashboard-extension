# GPU Monitoring in Jupyter Lab
[![Binder](https://binder.cern.ch/badge_logo.svg)](https://binder.cern.ch/v2/git/https%3A%2F%2Fgitlab.cern.ch%2Fbinder%2Frepos%2Fgpu-dashboard-extension/master) (button sends to a non-gpu node, to test the dashboard grab the container tag from the [registry](https://gitlab.cern.ch/binder/images/container_registry) and through [hub.cern.ch](https://hub.cern.ch) select a notebook server profile with a GPU)

Container with the Jupyter Lab [GPU dasboards extension](https://github.com/rapidsai/jupyterlab-nvdashboard) for nvidia graphics cards.
